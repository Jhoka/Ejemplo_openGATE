# Efectos Radiobiológicos de los Neutrones en Grandes Altitudes #

Este repositorio contiene lo necesario para llevar a cabo una simulación de los **Efectos radiobiológicos de los neutrones en grandes altitudes**.


**Objetivo**: hacer una simulación para estudiar el efecto radiobiológico de los neutrones sobre un _phantom_ de Pecho y uno de Piel. A su vez, para ambos tejidos se considera tanto la presencia de un _slab_ (fuselaje de un Airbus A380) como su ausencia, con la intención de obtener resultados realistas.
Aquí encontramos las carpetas:

- **Sin_SLAB**: para el caso de las simulaciones de Pecho y Piel en ausencia del fuselaje.
- **SLAB**: para el caso de las simulaciones de Pecho y Piel en presencia del fuselaje.


### PARA HACER LAS CORRIDAS

1. Entrar en la carpeta según la simulación que se desee hacer (Sin_SLAB o SLAB)
2. Se abre un terminal en la carpeta que se quiere correr. Ej: desde la carpeta Sin_SLAB
3. Una vez abierto, existen tres opciones:

	 3.1. Si se quieren realizar las dos corridas (ambos tejidos) de forma automática, se escribe el siguiente comando: **bash Corrida.sh > Corridas.txt**, donde la línea "Corrida.txt" es para obtener un archivo .txt donde veremos los mensajes que se muestran en el terminal durante la simulación.
	
	 3.2. Si se quiere correr un solo archivo (para Pecho o para Piel), se escribe el comando: **Gate mac/Pecho.mac**. 

	 3.3. Si se quiere visualizar el proceso de simulación en consola:

	 - Se escribe el comando **Gate --qt**.
	 - En la consola que se abre a continuación, se escribe el comando /control/execute mac/visu.mac para obtener detalles referentes a los ejes de coordenadas, tamaños, colores, entre otros.
	 - Se escribe el comando /control/execute mac/Pecho.mac, ya que es allí donde se encuentran las líneas necesarias para cada proceso. De allí se obtienen imágenes como las mostradas en la carpeta "imágenes" encontrada en la carpeta **"EjemploMWE"**.
	 - Finalizada la simulación, se escribe el comando exit. 
4. Una vez procesadas todas las corridas, debe haber 7 archivos para cada tejido en todas las carpetas "output", es decir, un total de 14 archivos:

	- Dose-Squared.txt
	- Dose-Uncertainty.txt
	- Dose.txt
	- DoseToWater.txt
	- Edep-Squared.txt
	- Edep-Uncertainty.txt
	- Edep.txt

5. Se escribe el comando **exit** para cerrar el terminal.


### PARA GRAFICAR LOS RESULTADOS


1. De la carpeta "output", se toman los archivos con el nombre "Pecho-Dose.txt" y "Pecho-Dose-Uncertainty.txt" y se ubican en la carpeta "Pecho" incluida en "procesamiento". Se hace lo mismo para los respectivos archivos de Piel.
2. Es **NECESARIO** verificar que, tanto en el script "DAE_pechopiel.py" como en "TDAE_pechopiel.py", se encuentre SIN COMENTAR el comando **plt.show()**, ya que de otro modo, la gráfica no se verá de inmediato.

	 2.1. Si se quieren obtener los resultados en formato tikz, el comando **tikz.save("DAE_pechopiel.tex", axis_height='10cm', axis_width='14cm')** ubicado al final del script debe estar SIN COMENTAR.
3. Desde "procesamiento" se abre un terminal y se escribe **python3 DAE_pechopiel.py** (en caso de querer graficar la dosis ambiental) o **python3 TDAE_pechopiel.py** (para la tasa de dosis ambiental).
4. Para cerrar el terminal, se usa el comando **exit**.
