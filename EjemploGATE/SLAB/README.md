Contiene cuatro carpetas:

1. **data**: contiene dos archivos

	 1.1. **Hist.txt:** espectro de neutrones, donde se indican las energías y proabilidades de cada una de ellas. Los tipos de histogramas que pueden usarse, pueden encontrarse en la guía del usuario de openGATE en su sección ___"Source"___.

	 1.2. **GateMaterials.db:** contiene una lista de elementos y materiales que pueden serle asignados tanto al _phantom_ como al _World_ o entorno de la simulación. Cabe destacar que si se conoce la composición química de un elemento que no se encuentre ya en esta lista, la misma se puede modificar. Para detalles, se debe consultar la guía del usuario de openGATE en su sección ___"Materials"___.
2. **mac**: contiene los siguientes archivos 

	 2.1. **SLABPecho.mac**  

	 2.2. **SLABPiel.mac**

	 En ambos archivos se encuentran todas las líneas de comando necesarias para que se lleve a cabo cada proceso, así como también las líneas de comando para modificar la geometría (tanto del _phantom_ como del _SLAB_), selección de materiales, el tipo y número de partículas, la física necesaria para la obtención de las mismas, y el tipo de salida en el que se desean obtener los resultados, así como la indicación de la carpeta en la cual se pueden buscar los resultados. Todos estos detalles pueden ser modificados según las necesidades del usuario.

	 2.3. **verbose.mac**: información sobre la cantidad de mensajes durante la visualización.

	 2.4. **visu.mac**: detalles pertinentes para una visualización por consola.
3. **output**: esta carpeta se encuentra inicialmente vacía. Una vez procesados los scripts, se generan en ella 7 documentos por tejido. Es importante tener en cuenta que los valores obtenidos variarán cada vez que se haga una nueva simulación.
4. **procesamiento**: contiene los archivos

	 4.1. **Pecho**: para copiar allí los resultados de _"SLABPecho-Dose.txt"_ y _"SLABPecho-Dose-Uncertainty.txt"_.

	 4.2 **Piel**: para copiar allí los resultados de _"SLABPiel-Dose.txt"_ y _"SLABPiel-Dose-Uncertainty.txt"_.

	 4.3. **DAE_pechopiel.py**: script que genera el gráfico de la dosis ambiental equivalente depositada en pecho y piel.
	 
	 4.4. **TDAE_pechopiel_py**: script que genera el gráfico de la dosis ambiental equivalente obtenida en pecho y piel.


### Nota ###

El archivo "Corrida.sh" permite realizar ambas simulaciones de manera automática. 

1. Se abre un terminal
2. Se escribe el comando **bash Corrida.sh > Corrida.txt**. La línea "Corrida.txt" genera un documento .txt donde se encuentran todos los mensajes mostrados en el terminal.
