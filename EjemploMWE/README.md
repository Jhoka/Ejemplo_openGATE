## M.W.E
Minimal Working Example

Este repositorio contiene lo necesario para realizar la simulación de irradiación con neutrones de un cilindro de agua contenido en una esfera de pulmón (Minimal Working Example). 

**Objetivo:** irradiar una cilindro de agua dentro de una esfera de pulmón para estudiar la dosis depositada. 

Aquí encontramos las carpetas: 

- **data**: contiene el archivo _GateMaterials.db_ correspondiente a la base de datos de los materiales seleccionados.  
- **mac**: donde se encuentran los scripts _Ejemplo.mac_ (script principal), _verbose.mac_ y _visu.mac_.
- **output**: inicialmente vacío. En ella se registran los resultados de la simulación. 
- **imágenes**: contiene imágenes ejemplos de la simulación hecha desde la consola de visualización.

Este ejemplo fue desarrollado haciendo uso de la versión 8.0 de la máquina virtual vGATE, la cual puede descargarse en: http://www.opengatecollaboration.org/ 


Pasos para la simulación:

1. Se abre un terminal en la carpeta desde donde se quiera correr la simulación. **Ej:** desde la carpeta **EjemploMWE**.
2. Se escribe en el terminal el comando **Gate mac/Ejemplo.mac**. 

	 2.1 En caso de querer una visualización por consola (como las mostradas en la carpeta "imágenes"), los pasos son los siguientes:
	- Se escribe en el terminal el comando **Gate --qt**
	- Esto desplegará una consola de visualización, donde el primer comando que debe escribirse es **/control/execute mac/visu.mac**, encargado de generar detalles sobre los ejes.
	- Finalmente se escribe el comando **/control/execute mac/Ejemplo.mac**, donde se va viendo el proceso de simulación.
3. Una vez procesadas todas las corridas, debe haber 7 archivos:

	+ Dose-Squared.txt
	+ Dose-Uncertainty.txt
	+ Dose.txt
	+ DoseToWater.txt
	+ Edep-Squared.txt
	+ Edep-Uncertainty.txt
	+ Edep.txt

4. Se escribe el comando **exit** para cerrar el terminal.  


### Detalles

Los materiales seleccionados, así como la cantidad de partículas pueden modificarse en el script principal _Ejemplo.mac_. 
